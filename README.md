# Sentinel Download

For fast search, filter and download Sentinel satellite images from the Copernicus Open Access Hub easy.

Prerequisite modules: numpy, pandas, sentinelsat

First set up the filter text files. [Attributes mentioned in block brackkets are mandatory]. Optional attributes can be deteted if required, but the filter should not have an empty line. The reading opration of my tool is not roboust enough. For details refer to the sample filter files.
>> Attributes --> [userID], [password], [outFolder], [platformName], [fromDate], [toDate], Area (in WKT), productType and relativeOrbitNumber are common for S1 and S2.
>> Attribute --> cloudCoverPercentage exist only for S2. 
>> Attributes --> orbitDirection, sensorOperationalMode and polarisationMode exist only for S1.


Create a filter and navigate to the folder containing the .py files, and open the command prompt/terminal. Execute the following commands:

>>  python s1Downlaod list path_to_filter(s)
This will create a CSV of available scenes in the specified output folder. This file can be edited, and scenes not required can be deleted. Do not rename the file or leave empty lines.

>>  python s1Downlaod download path_to_filter(s)
This will download the available files in the CSV list.


Use the below link to get WKT of the geographic bounds (to specify the optional area attribute):  
https://arthur-e.github.io/Wicket/sandbox-gmaps3.html


## Happy Downloading